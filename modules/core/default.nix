{
  self,
  config,
  lib,
  pkgs,
  inputs,
  ...
}: {
  system.stateVersion = "22.05";

  environment = {
    systemPackages = with pkgs; [
      # CLI Utilities
      curl
      wget
      git
      jq
      bc
      fd
      ripgrep
      whois
      httpie
      htop
      git
      bat
      axel
      tokei
      fzf
      tree
      du-dust
      tealdeer
      ffmpeg
      bemenu
      nix-index
      neofetch
      age
      usbutils
      dig
      asciinema
      fwupd
      nix-du
      comma
      nix-zsh-completions
      zsh-completions
      atool

      # Archiving
      p7zip
      unzip
      unrar
      rar
      zip
      awscli2
      keychain

      # Nix Tools
      cachix
      manix
      inputs.home-manager.packages.x86_64-linux.home-manager
    ];

    sessionVariables = {
      XAUTHORITY = ''"$XDG_RUNTIME_DIR"/Xauthority '';
      ERRFILE = ''"$XDG_CACHE_HOME/X11/xsession-errors" '';
      XCOMPOSECACHE = ''"$XDG_CACHE_HOME"/X11/xcompose '';
    };

    localBinInPath = true;
    pathsToLink = ["/libexec" "/share/zsh"];
  };

  nix = {
    registry = {
      nixpkgs.flake = inputs.nixpkgs;
      master.flake = inputs.nixpkgs-master;
      unstable.flake = inputs.nixpkgs-unstable;
      home-manager.flake = inputs.home-manager;
      self.flake = inputs.self;
    };
    nixPath = [
      "nixpkgs=${inputs.nixpkgs}"
      "home-manager=${inputs.home-manager}"
    ];

    gc.automatic = true;
    gc.dates = "weekly";
    gc.options = "--delete-older-than 30d";
    package = inputs.nix.packages.x86_64-linux.nix;
    optimise.automatic = true;
    settings = {
      sandbox = true;
      allowed-users = ["@wheel"];
      trusted-users = ["root" "@wheel"];
      auto-optimise-store = true;
      fallback = true;
      keep-outputs = true;
      keep-derivations = true;
      experimental-features = ["nix-command" "flakes" "ca-derivations"];
      substituters = [
        "https://nix-community.cachix.org/"
      ];

      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
    };
  };

  nixpkgs.config.allowUnfreePredicate = pkg: true;

  programs.neovim.enable = true;
  programs.neovim.defaultEditor = true;

  programs.command-not-found.enable = false;

  services.earlyoom.enable = true;
}
