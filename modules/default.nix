{
  imports = [
    ./core
    ./gaming
    ./desktops
    ./printing
    ./vm-tools
  ];
}
