{ lib, config, pkgs, mypkgs, upkgs, ... }:
let
  cfg = config.desktops.sway;
  inherit (lib) mkIf mkEnableOption;
in
{
  options.desktops.sway = {
    enable = mkEnableOption "Sway Window Manager";
  };

  config = mkIf cfg.enable {

    programs.sway = {
      enable = true;
      extraSessionCommands = ''
        # Fix for some Java AWT applications (e.g. Android Studio),
        # use this if they aren't displayed properly:
        export _JAVA_AWT_WM_NONREPARENTING=1
        export MOZ_ENABLE_WAYLAND=1
        export XDG_CURRENT_DESKTOP="sway"
        export QT_QPA_PLATFORMTHEME=qt5ct
        export QT_QPA_PLATFORM=wayland
      '';

      wrapperFeatures.gtk = true; # so that gtk works properly
      extraPackages = with pkgs; [
        swayidle
        waybar
        upkgs.i3status-rust
        j4-dmenu-desktop
        bemenu
        xwayland
        wl-clipboard
        clipman
        swaylock-fancy
        dunst
        light
        rofi
        wdisplays
        sway-contrib.grimshot
        wf-recorder
        slurp
        acpi
      ];
    };
    programs.xwayland.enable = true;
  };
}
