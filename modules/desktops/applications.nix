{
  pkgs,
  lib,
  config,
  mypkgs,
  mpkgs,
  upkgs,
  ...
}: {
  services.gnome.sushi.enable = true;
  services.packagekit.enable = true;
  services.system-config-printer.enable = false;
  networking.firewall = let
    allowedRanges = [
      {
        from = 1714;
        to = 1764;
      }
    ];
  in {
    allowedTCPPortRanges = allowedRanges;
    allowedUDPPortRanges = allowedRanges;
  };
  environment = {
    systemPackages = with pkgs; [
      # Themes
      gnome.adwaita-icon-theme
      qt5.qtwayland

      gnome.gnome-themes-extra
      gsettings-desktop-schemas

      # Terminal
      (pkgs.writeShellScriptBin "alacritty" "WAYLAND_DISPLAY= ${alacritty}/bin/alacritty")
      alacritty


      # Browsers
      firefox

      # Media
      vlc
      youtube-dl
      blueman
      pavucontrol
      pulseaudio

      # Password Managers
      keepassxc

      # Etc
      libreoffice
      gnome.nautilus
      gnome-console
      nautilus-open-any-terminal
      gnome.file-roller
      input-remapper
      zotero
      fragments
      evince
      zathura
      polkit_gnome
      libsForQt5.kdeconnect-kde
      qbittorrent
      calibre
      texlive.combined.scheme-full
      pandoc
      nushell
    ];
  };
}
