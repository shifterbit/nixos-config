{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.desktops.awesome;
in
{

  options.desktops.awesome = {
    enable = mkEnableOption "Awesome Window Manager";
  };

  config = mkIf cfg.enable {
    services.xserver.windowManager.awesome.enable = true;

    environment.systemPackages = with pkgs; [
      acpi
      alacritty
      rofi
      feh
      nitrogen
      i3lock-fancy
      mypkgs.dmenu
      mpd
      mpc_cli
      scrot
      unclutter
      xsel
      slock
      glib
    ];

    programs.light.enable = true;
  };
}
