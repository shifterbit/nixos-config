{ lib, config, pkgs, mypkgs,upkgs ,... }:
let
  cfg = config.desktops.i3;
  inherit (lib) mkIf mkEnableOption;
in
{
  options.desktops.i3 = {
    enable = mkEnableOption "Sway Window Manager";
  };

  config = mkIf cfg.enable {

    services.xserver.windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [
        dunst
        light
        upkgs.i3status-rust
        bemenu
        caffeine-ng
        feh
        gnome.gnome-screenshot
        flameshot
        i3lock-fancy
        xclip
        polybarFull
        networkmanager_dmenu
        polkit_gnome
        rofi
      ];
    };
  };
}
