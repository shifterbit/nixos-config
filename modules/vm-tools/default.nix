{
  lib,
  config,
  pkgs,
  ...
}: let
  inherit (lib) mkIf mkEnableOption;
  cfg = config.vm-tools;
in {
  options.vm-tools = {
    enable = mkEnableOption "virtualization tools";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      virt-manager
      qemu_kvm
    ];

    virtualisation.spiceUSBRedirection.enable = true;

    virtualisation.podman.enable = true;
    virtualisation.docker = {
        enable = true;
        rootless = {
          enable = true;
          setSocketVariable = true;
        };
    };
    virtualisation.libvirtd = {
        enable = true;
      };
  };
}
