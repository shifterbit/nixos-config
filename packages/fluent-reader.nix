{
  appimageTools,
  makeDesktopItem,
  stdenvNoCC,
}: let
  fluent-reader = appimageTools.wrapType1 {
    name = "fluent-reader";
    src = builtins.fetchurl {
      url = "https://github.com/yang991178/fluent-reader/releases/download/v1.1.2/Fluent.Reader.1.1.2.AppImage";
      sha256 = "17l3xiwvl8pk2kb4gp90ivjyxf926indf6gb4b8m8agzwx2d3s4s";
    };
  };

  desktopItem = makeDesktopItem {
    name = "fluent-reader";
    desktopName = "Fluent Reader";
    comment = "RSS Feed Reader";
    exec = "${fluent-reader}/bin/fluent-reader %u";
    mimeTypes = ["application/rss+xml"];
  };
in
  stdenvNoCC.mkDerivation {
    pname = "fluent-reader";
    version = "1.1.2";
    src = fluent-reader;
    installPhase = ''
      mkdir -p $out/bin $out/share/applications
      install -m 755 $src/bin/fluent-reader $out/bin
      install -m 444 -D ${desktopItem}/share/applications/* -t $out/share/applications
    '';
  }
