{
  vimUtils,
  fetchFromGitHub,
}:
vimUtils.buildVimPlugin {
  name = "cmp-nixpkgs";
  namePrefix = "";
  src = fetchFromGitHub {
    owner = "gravndal";
    repo = "cmp-nixpkgs";
    rev = "12bebfe528ecabf65ddab5d50521487cc413ebca";
    sha256 = "sha256-kRdXFhcrAzWFK3CVt5BoQKO2HkGH2ujmtNOFX6uo9jQ=";
  };
}
