{
  appimageTools,
  makeDesktopItem,
  stdenvNoCC,
  libthai,
}: let
  cinny-desktop = appimageTools.wrapType1 {
    name = "cinny-desktop";
    src = builtins.fetchurl {
      url = "https://github.com/cinnyapp/cinny-desktop/releases/download/v2.0.4/cinny_2.0.4_amd64.AppImage";
      sha256 = "1psilz3vg8r9i3gxxa5s82pz3bscw8lj6viqxjx3c7pcqiak557m";
    };
    extraPkgs = pkgs: [libthai];
  };

  desktopItem = makeDesktopItem {
    name = "cinny-desktop";
    desktopName = "Cinny Desktpp";
    comment = "Matrix Client";
    exec = "${cinny-desktop}/bin/cinny-desktop %u";
  };
in
  stdenvNoCC.mkDerivation {
    pname = "cinny-desktop";
    version = "2.0.4";
    src = cinny-desktop;
    installPhase = ''
      mkdir -p $out/bin $out/share/applications
      install -m 755 $src/bin/cinny-desktop $out/bin
      install -m 444 -D ${desktopItem}/share/applications/* -t $out/share/applications
    '';
  }
