{
  vimUtils,
  fetchFromGitHub,
}:
vimUtils.buildVimPlugin {
  namePrefix = "";
  name = "dracula.nvim";
  src = fetchFromGitHub {
    owner = "Mofiqul";
    repo = "dracula.nvim";
    rev = "40d38e95bf006470b3efe837b2e0b9f66707c850";
    sha256 = "sha256-kKjYiy99nkDtQPSpIfVoizkuAVZXCgIf7bdKIUtBRTo=";
  };
}
