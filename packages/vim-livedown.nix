{
  vimUtils,
  fetchFromGitHub,
}:
vimUtils.buildVimPlugin {
  namePrefix = "";
  name = "vim-livedown";
  src = fetchFromGitHub {
    owner = "shime";
    repo = "vim-livedown";
    rev = "24c78c0c1177c0231bb542fa1e3807a3b089248e";
    sha256 = "sha256-zVKwYPuHeN41beiNLQO0DLfB5knqwC1UunNDFwns0DY=";
  };
}
