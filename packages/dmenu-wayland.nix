{
  dmenu-wayland,
  fetchpatch,
}:
dmenu-wayland.overrideAttrs (
  old: {
    patches = old.patches ++ [
      /* (fetchpatch { */
      /*   url = "https://tools.suckless.org/dmenu/patches/center/dmenu-center-4.8.diff"; */
      /*   sha256 = "0z21l82y11rx0kd74abpyh925rq29dl34y7p4868dl3ffknc7ifz"; */
      /* }) */
      (fetchpatch {
        url = "https://tools.suckless.org/dmenu/patches/fuzzymatch/dmenu-fuzzymatch-4.9.diff";
        sha256 = "0yababzi655mhpgixzgbca2hjckj16ykzj626zy4i0sirmcyg8fr";
      })
      (fetchpatch {
        url = "https://tools.suckless.org/dmenu/patches/dracula/dmenu-dracula-20211128-d78ff08.diff";
        sha256 = "1f46z4m0x29j935ciygpzh37cz16969069a4hw0dj2yxbjlwrkxf";
      })
      (fetchpatch {
        url = "https://tools.suckless.org/dmenu/patches/mouse-support/dmenu-mousesupport-5.1.diff";
        sha256 = "0idwscv04qc96w7hgnndl16n5hxdp7a8sggsb5ain13din373qci";
      })
      (fetchpatch {
        url = "https://tools.suckless.org/dmenu/patches/prefix-completion/dmenu-prefixcompletion-4.9.diff";
        sha256 = "0a685fiyyry2ism2yf9y21w06nw4fr5lv402r1dcb32qv2vzk2m9";
      })
    ];
  }
)
