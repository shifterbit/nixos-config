{
  vimUtils,
  fetchFromGitHub,
  pkgs,
}:
vimUtils.buildVimPlugin {
  name = "nvim-dap-go";
  namePrefix = "";
  src = fetchFromGitHub {
    owner = "leoluz";
    repo = "nvim-dap-go";
    rev = "c75921726ccfe97070285f206de49eddff276ea5";
    sha256 = "sha256-utDon+npwT1wT+clL+y+pau515hCUdeqvfUDPucu/+k=";
  };
}
