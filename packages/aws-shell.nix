{python37Packages}: let
  inherit (python37Packages) buildPythonApplication fetchPypi;
  pname = "aws-shell";
  version = "0.2.2";
in
  buildPythonApplication {
    inherit pname version;

    src = fetchPypi {
      inherit pname version;
      sha256 = "sha256-/RaZ6l8gHny6rK6zS/HriMj+bcaySLzhs9IrPgmaQeU=";
    };
  }
