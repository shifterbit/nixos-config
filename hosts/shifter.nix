{
  config,
  lib,
  pkgs,
  ...
}: {
  imports = [../modules];

  boot = {
    initrd.luks.devices = {
      crypt = {
        device = "/dev/nvme0n1p2";
        allowDiscards = true;
        preLVM = true;
      };
    };
  };
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.availableKernelModules = ["xhci_pci" "ahci" "nvme"];
  boot.initrd.kernelModules = ["dm-snapshot"];
  boot.kernelModules = ["kvm-intel"];
  boot.extraModulePackages = [];
  boot.kernelPackages = pkgs.linuxPackages;

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/0554d6af-3b95-4f91-9558-db15526b0a03";
    fsType = "ext4";
  };

  fileSystems."/nix/store" = {
    device = "/nix/store";
    fsType = "none";
    options = ["bind"];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/28F0-C892";
    fsType = "vfat";
  };

  swapDevices = [
    {device = "/dev/disk/by-uuid/c0e1325c-437f-4123-b5c7-85067476e92d";}
  ];

  hardware.enableRedistributableFirmware = true;

  boot.cleanTmpDir = true;
  services.fstrim.enable = true;
  services.power-profiles-daemon.enable = false;
  services.tlp.enable = true;

  powerManagement.powertop.enable = true;
  hardware.cpu.intel.updateMicrocode = true;
  systemd.oomd.enable = false;

  networking.hostName = "shifter";
  users.users.tek = {
    uid = 1000;
    description = "Tek";
    isNormalUser = true;
    shell = pkgs.bash;
    extraGroups = [
      "wheel"
      "networkmanager"
      "kvm"
      "audio"
      "video"
      "input"
      "uinput"
      "bluetooth"
      "libvirtd"
      "wireshark"
      "gamemode"
    ];
  };

  # Some Network stuff
  networking.networkmanager.enable = true;
  systemd.services.NetworkManager-wait-online.enable = false;
  networking.networkmanager.ethernet.macAddress = "random";
  networking.networkmanager.wifi.macAddress = "random";
  networking.interfaces.wlp0s20f3.mtu = 1452;
  networking.enableIPv6 = false;

  networking.hosts = {"127.0.0.1" = ["localhost" "dev.local" "home.local" "syncthing.local"];};

  services.gvfs.enable = true;

  services.nginx = {
    enable = true;
    virtualHosts = {
      "syncthing.local" = {
        locations."/" = {
          proxyPass = "http://127.0.0.1:8384";
        };
      };
      "dev.local" = {
        locations."/" = {
          proxyPass = "http://127.0.0.1:4000";
        };
      };
    };
  };

  services.dnscrypt-proxy2 = {
    enable = true;
    upstreamDefaults = true;
    settings = {
      server_names = [
        "mullvad-adblock-doh"
        "quad9-dnscrypt-ip4-filter-ecs-pri"
        "controld-block-malware-ad"
      ];
    };
  };

  systemd.services.dnscrypt-proxy2.serviceConfig = {
    StateDirectory = "dnscrypt-proxy";
  };

  hardware.bluetooth.enable = true;

  desktops = {
    enable = true;
    gnome.enable = true;
    i3.enable = true;
    sway.enable = true;
  };
  vm-tools.enable = true;
  gaming.enable = true;

  services.fwupd.enable = true;
  programs.ssh.startAgent = true;
  services.openssh.enable = true;
  services.openssh.passwordAuthentication = false;
  programs.ssh.askPassword = "${pkgs.lxqt.lxqt-openssh-askpass}/bin/lxqt-openssh-askpass";

  services.fprintd.enable = true;
  services.fprintd.tod.enable = true;
  services.fprintd.tod.driver = pkgs.libfprint-2-tod1-goodix;

  services.xserver.libinput.enable = true;
  services.xserver.libinput.touchpad.tapping = true;
  services.xserver.libinput.touchpad.clickMethod = "buttonareas";

  services.logind.extraConfig = ''
    HandlePowerKey=suspend
    HandleLidSwitch=ignore
  '';
  programs.wireshark.enable = true;
  powerManagement.enable = true;
  environment = let
    remapScript = ''
      ${pkgs.input-remapper}/bin/input-remapper-control --preset "NewAlt" --device "AT Translated Set 2 keyboard" --command start --config /home/tek/.config/input-remapper '';
  in {
    shellAliases = {
      "remap" = remapScript;
    };
    shellInit = "(${remapScript} > /dev/null 2>&1- &)";
    systemPackages = with pkgs; [nmap wireshark aircrack-ng ventoy-bin tlp];
  };
}
