{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-master.url = "github:nixos/nixpkgs";
    nix.url = "github:nixos/nix";

    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs-unstable";
    nix-software-center.url = "github:vlinkz/nix-software-center";
    nixpkgs-wayland .url = "github:nix-community/nixpkgs-wayland";
    alejandra.url = "github:kamadorueda/alejandra/1.4.0";
    nil-lsp.url = "github:oxalica/nil";

    nixpkgs-wayland.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
    self,
    home-manager,
    nixpkgs,
    ...
  } @ inputs: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
    spkgs = pkgs;
    upkgs = import inputs.nixpkgs-unstable {
      inherit system;
      config.allowUnfree = true;
    };
    mpkgs = import inputs.nixpkgs-master {
      inherit system;
      config.allowUnfree = true;
    };
    mypkgs = import ./packages {
      inherit inputs self;
    };
    extraArgs = {
      inherit inputs mypkgs mpkgs upkgs spkgs;
    };
  in {
    packages.${system} = mypkgs;
    nixosConfigurations = {
      shifter = nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = extraArgs;
        modules = [
          ./hosts/shifter.nix
        ];
      };
    };
    homeConfigurations.tek = home-manager.lib.homeManagerConfiguration {
      pkgs = upkgs;
      modules = [./home];
      extraSpecialArgs = extraArgs;
    };
  };
}
