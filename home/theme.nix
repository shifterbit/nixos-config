{
  pkgs,
  mypkgs,
  config,
  ...
}: {
  home.file = {
    "Pictures/wallpapers" = {
      source = ../assets/wallpapers;
      recursive = true;
    };
  };
  gtk = {
    enable = false;
    gtk2.configLocation = "${config.xdg.configHome}/gtk-2.0/gtkrc";
    theme = {
      name = "adw-gtk3";
      package = mypkgs.adw-gtk3;
    };
    iconTheme = {
      name = "Adwaita";
      package = pkgs.gnome.adwaita-icon-theme;
    };
    cursorTheme = {
      name = "Adwaita";
      package = pkgs.gnome.adwaita-icon-theme;
    };
  };
}
