{pkgs, ...}: {
  programs.tmux = {
    enable = true;
    prefix = "C-z";
    keyMode = "vi";
    tmuxp.enable = true;
    baseIndex = 1;
    historyLimit = 100000;
    sensibleOnTop = true;
    terminal = "screen-256color";
    extraConfig = ''
      set -ga terminal-overrides ",*:Tc"
      set -g @shell_mode 'vi'
      set -g mouse on
    '';
    plugins = with pkgs; [
      {plugin = tmuxPlugins.yank;}
      {plugin = tmuxPlugins.tmux-fzf;}
      {plugin = tmuxPlugins.nord;}
    ];
  };
}
