{ pkgs, ... }: {

  home.file = {
    ".config/i3/config" = {
      source = ./config;
    };
    ".config/polybar" = {
      source = ./polybar;
      recursive = true;
    };
  };
}
