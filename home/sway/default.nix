{ pkgs, ... }: {

  home.file = {
    ".config/sway/config" = {
      source = ./config;
    };
    ".config/waybar" = {
      source = ./waybar;
      recursive = true;
    };

  };
}
