(advice-add 'load-file :override
            (lambda (file) (load (expand-file-name file) nil t t)))
(setq gc-cons-threshold most-positive-fixnum 
      gc-cons-percentage 0.6)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(set-face-attribute 'default nil  :family "Hack Nerd Font" :height 130)

