;;; init.el --- MyConfig  -*- lexical-binding: t; coding:utf-8; -*-
(add-hook 'emacs-startup-hook
	  (lambda ()
	    (setq gc-cons-threshold (* 100 1024 1024) ;; 100mb
		  gc-cons-percentage 0.1)))

(setq read-process-output-max (* 16 1024 1024)) ;; 16mb
(setq native-comp-async-report-warnings-errors nil)

(defun my-minibuffer-setup-hook ()
  (setq gc-cons-threshold most-positive-fixnum))

(defun my-minibuffer-exit-hook ()
  (setq gc-cons-threshold (* 100 1024 1024)))

(add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)

;;; Emacs Configuration
;;;; Speed up startup
(setq package-native-compile t)
(defun sh/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                    (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'sh/display-startup-time)



(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(setq package-selected-packages
      '(use-package
	 keychain-environment
	 try
	 ;; Literring
	 no-littering
	 ;; Benchmarking
	 esup
	 ;; Bindings
	 general hydra
	 ;; UI
	 all-the-icons all-the-icons-completion
	 doom-modeline doom-themes minions which-key
	 ;; Editing
	 evil evil-collection evil-nerd-commenter
	 undo-tree lispyville
	 ;; Git
	 magit
	 ;;  Vert&co
	 vertico marginalia consult orderless embark prescient
	 vertico-prescient embark-consult consult-flycheck
	 consult-lsp
	 ;; IDE Features
	 company company-quickhelp yasnippet
	 yasnippet-snippets lsp-mode lsp-ui
	 dap-mode flycheck direnv eldoc-box
	 format-all eat
	 ;; Language Support
	 haskell-mode rustic nix-mode go-mode
	 js2-mode typescript-mode jsonian cider
	 elixir-mode web-mode auctex))

(package-install-selected-packages)





;;; General Configuration
(use-package emacs
  :config
  (global-auto-revert-mode 1)
  ;; Look and Feel
  (setq visible-bell t)
  (setq-default initial-scratch-message nil)
  (setq inhibit-startup-message t)
  (setq inhibit-compacting-font-caches t)

  ;; Files
  (setq find-file-visit-truename t)
  (setq global-auto-revert-non-file-buffers t)
  (setq vc-follow-symlinks t)
  (setq backup-directory-alist
        `((".*" . ,temporary-file-directory)))
  (setq auto-save-file-name-transforms
        `((".*" ,temporary-file-directory t)))
  (defalias 'yes-or-no-p 'y-or-n-p)
  (setq custom-file (locate-user-emacs-file "custom.el"))
  (load custom-file 'noerror 'nomessage)

  ;; Editing
  (setq backward-delete-char-untabify-method 'untabify)
  (setq tab-width 4)
  (defvaralias 'c-basic-offset 'tab-width)
  (defvaralias 'cperl-indent-level 'tab-width)
  (setq tab-always-indent 'complete)
  (electric-pair-mode 1)
  (show-paren-mode 1)
  (setq show-paren-delay 0)
  (global-display-line-numbers-mode)
  (dolist (mode '(org-mode-hook term-mode-hook eshell-mode-hook))
    (add-hook mode (lambda () (display-line-numbers-mode 0))))

  ;; Keybind
  (global-set-key "\C-x\C-m" 'execute-extended-command)
  (global-set-key "\C-c\C-m" 'execute-extended-command)

  (savehist-mode +1)
  ;; Tab bar mode
  (tab-bar-mode +1)
  )
(defun set-indentation (indentation)
  "Set indentation"
  (interactive "nTab Width: ")
  (setq tab-width indentation))

(defun enable-tabs ()
  "Enables Indentation with Tabs"
  (interactive)
  (indent-tabs-mode t)
  (setq tab-width 4))

(defun disable-tabs (indentation)
  "Disables Indentation With Tabs using spaces instead"
  (interactive "nTab Width (in spaces): ")
  (indent-tabs-mode nil)
  (setq tab-width indentation))

(defun two-space-indent () (lambda () (disable-tabs 2)))
(defun four-space-indent () (lambda () (disable-tabs 4)))

(add-hook 'prog-mode-hook #'two-space-indent)


;;; Package Configuration
;;;; No Littering
(use-package no-littering
  :demand t
  :config
  (setq auto-save-file-name-transforms
	`((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))

;;;; General Key Definitions

(use-package general
  :after evil
  :config
  (general-evil-setup)
  (general-create-definer sh/leader-key
    :keymaps '(normal visual)
    :prefix "SPC"
    :global-prefix "C-SPC")

  (sh/leader-key
    "SPC" 'execute-extended-command
    "eb"  'eval-buffer
    "er"  'eval-region)
  (provide 'leader-keys))



;;;; User Interface & Themes
(use-package doom-themes
  :demand t
  :init
  (setq doom-gruvbox-padded-modeline t)
  (load-theme 'doom-gruvbox t)
  :config
  (doom-themes-visual-bell-config)
  (doom-themes-org-config))

(use-package doom-modeline
  :demand t
  :init
  (doom-modeline-mode)
  :config
  (setq
   doom-modeline-icon nil
   doom-modeline-lsp nil
   doom-modeline-workspace-name t
   doom-modeline-minor-modes t))


(use-package minions
  :config
  (minions-mode +1))


(use-package all-the-icons
  :disabled
  :demand t
  )
(use-package all-the-icons-completion
  :disabled
  :after all-the-icons
  :config
  (all-the-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'all-the-icons-completion-marginalia-setup))

;;;; Which Key
(use-package which-key
  :defer 0.2
  :config
  (which-key-mode 1)
  (setq which-key-idle-delay 0.2))

;;;; Code Completion
(use-package yasnippet
  :defer t
  :config
  (yas-global-mode 1))

(use-package company
  :defer 0.01
  :config
  (global-company-mode)
  (with-eval-after-load 'orderless
    (defun company-completion-styles (capf-fn &rest args)
      (let ((completion-styles '(basic partial-completion)))
	(apply capf-fn args)))
    (advice-add 'company-capf :around #'company-completion-styles))

  (setq
   company-minimum-prefix-length 1
   company-auto-commit nil
   company-show-quick-access nil
   company-idle-delay 0
   company-selection-wrap-around t)
  )
(use-package company-quickhelp
  :after company
  :config
  (company-quickhelp-mode)
  )
;;;; Vert&co
(use-package vertico
  :demand t
  :config
  (vertico-mode 1)
  (setq vertico-cycle t))

(use-package orderless
  :demand t
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(use-package marginalia
  :demand t
  :config
  (marginalia-mode 1))

(use-package consult
  :demand t
  :config
  (global-set-key (kbd "C-c f f") 'consult-find)
  (global-set-key (kbd "C-c f r") 'consult-ripgrep)
  (global-set-key (kbd "C-x b") 'consult-buffer)
  (global-set-key (kbd "C-s") 'consult-line)

  (with-eval-after-load 'project
    (setq project-switch-commands (remove '(project-find-file "Find file") project-switch-commands))
    (setq project-switch-commands (remove '(project-find-regexp "Find regexp") project-switch-commands))
    (setq project-switch-commands (remove '(project-find-dir "Find directory") project-switch-commands))

    (add-to-list 'project-switch-commands '(consult-find "Find file" ?f))
    (add-to-list 'project-switch-commands '(consult-ripgrep "Find regexp" ?r))
    )


  ;; Make Consult feel a bit snappier
  (setq
   consult-async-refresh-delay 0.2
   consult-async-min-input 1
   consult-async-input-throttle 0
   consult-async-input-debounce 0)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  (with-eval-after-load 'leader-keys
    (sh/leader-key
      "ff" 'consult-find
      "fr" 'consult-ripgrep
      "fb" 'consult-buffer)))


(use-package embark
  :commands (embark-dwim embark-act embark-act-all)
  :init
  (global-set-key (kbd "C-;") 'embark-act)
  (global-set-key (kbd "C-:") 'embark-act-all)
  (global-set-key (kbd "C-'") 'embark-dwim)
  :config

  (setq prefix-help-command #'embark-prefix-help-command)
  (setq embark-prompter 'embark-completing-read-prompter)
  (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target))

(use-package embark-consult
  :after (consult embark)
  :config
  (add-hook 'embark-collect-mode-hook #'consult-preview-at-point-mode))

;;;; Magit
(use-package magit
  :commands (magit-status magit-project-status)
  :init
  (with-eval-after-load 'project
    (define-key project-prefix-map [remap project-vc-dir] #'magit-status)
    (setq project-switch-commands (remove '(project-vc-dir "VC-Dir") project-switch-commands))
    (add-to-list 'project-switch-commands '(magit-project-status "Magit" ?m))))

(with-eval-after-load 'leader-keys
  (sh/leader-key
    "g" 'magit-status))
;;;; Evil

(defun evil-show-doc ()
  (interactive)
  (cond
   ((bound-and-true-p lsp-ui-mode) (lsp-ui-doc-show))
   ((bound-and-true-p eldoc-mode) (eldoc-box-help-at-point))
   (t (evil-lookup)))
  )

(use-package eldoc-box
  :demand t
  :config
  (require 'eldoc-box))

(use-package evil
  :demand t
  :init
  (setq evil-want-keybinding nil)
  :config
  (evil-define-key 'normal 'prog-mode-map (kbd "K") #'evil-show-doc)
  (evil-define-key 'normal 'prog-mode-map (kbd "gD") #'xref-find-definitions)

  (evil-mode 1))

(use-package undo-tree
  :after evil
  :config
  (global-undo-tree-mode)
  (evil-set-undo-system 'undo-tree))

(use-package evil-nerd-commenter
  :after evil
  :config
  (define-key evil-normal-state-map "gcc" 'evilnc-comment-or-uncomment-lines)
  (define-key evil-visual-state-map "gcc" 'evilnc-comment-or-uncomment-lines))

(use-package evil-collection
  :after evil
  :config
  (setq evil-collection-mode-list (remove 'go-mode evil-collection-mode-list))
  (evil-collection-init))


;;;; Parens
(use-package lispyville
  :commands lispyville-mode
  :init
  (add-hook 'emacs-lisp-mode-hook #'lispyville-mode)
  :config
  (lispyville-set-key-theme
   '((operators normal)
     c-w
     (prettify insert)
     (atom-movement normal visual motion)
     (text-objects normal visual motion)
     commentary
     wrap
     slurp/barf-lispy
     additional
     (additional-movement normal visual motion)
     additional-insert))
  )

;;;; Flycheck
(use-package flycheck
  :defer t
  :init (global-flycheck-mode)
  (with-eval-after-load 'leader-keys
    (sh/leader-key
      "x" 'consult-flycheck)
    )

  )
(use-package consult-flycheck
  :after (flycheck consult))
(use-package consult-lsp
  :after (consult lsp-mode))

;;;; LSP/General Code

(use-package format-all :defer t)
(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :config
  (setq lsp-completion-provider :none)
  (setq lsp-keymap-prefix "C-c l")
  (setq lsp-eldoc-render-all t)
  (setq lsp-enable-snippet t)
  (setq lsp-nix-nil-formatter '("alejandra"))
  (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration))

(use-package lsp-ui
  :after lsp-mode
  :config
  (setq lsp-ui-doc-position 'at-point)
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references))

(defun lookup-code-symbols ()
  (interactive)
  (cond
   ((bound-and-true-p lsp-mode) (call-interactively 'consult-lsp-symbols))
   ((bound-and-true-p eglot--managed-mode) (call-interactively 'consult-eglot-symbols))))

(defun code-rename ()
  (interactive)
  (cond
   ((bound-and-true-p lsp-mode) (call-interactively 'lsp-rename))
   ((bound-and-true-p eglot--managed-mode) (call-interactively 'eglot-rename))))

(defun code-format ()
  (interactive)
  (cond
   ((bound-and-true-p lsp-mode) (call-interactively 'lsp-format-buffer))
   ((bound-and-true-p eglot--managed-mode) (call-interactively 'eglot-format))))

(defun code-format-all ()
  (interactive)
  (format-all-buffer 'always))


(defun execute-code-action ()
  (interactive)
  (cond
   ((bound-and-true-p lsp-mode) (call-interactively 'lsp-execute-code-action))
   ((bound-and-true-p eglot--managed-mode) (call-interactively 'eglot-code-actions))))

(with-eval-after-load 'leader-keys
  (sh/leader-key
    "ca" 'execute-code-action
    "cj" 'lookup-code-symbols
    "cr" 'code-rename
    "cf" 'code-format-all
    "cF" 'code-format)
  )

;;;; Eat(TERM)
(use-package eat
  :config
  (add-hook 'eshell-load-hook #'eat-eshell-mode)
  ;; For `eat-eshell-visual-command-mode'.
  (add-hook 'eshell-load-hook #'eat-eshell-visual-command-mode)

  (with-eval-after-load 'project
    (setq project-switch-commands (remove '(project-eshell "Eshell") project-switch-commands))
    (add-to-list 'project-switch-commands '(eat-project "Terminal" ?t)))

  )

;;;; Direnv
(use-package direnv
  :config
  (direnv-mode 1))


;;;; Project
;;;; Debugging
(use-package dap-mode
  :after lsp
  :init
  (add-hook 'dap-stopped-hook
	    (lambda (arg) (call-interactively #'dap-hydra)))
  :config
  (setq dap-auto-configure-features '(sessions locals controls tooltip))
  (require 'dap-dlv-go)
  (require 'dap-gdb-lldb)
  (require 'dap-node)
  (require 'dap-elixir)
  (dap-register-debug-template "Rust::GDB Run Configuration"
			       (list :type "gdb"
                                     :request "launch"
                                     :name "GDB::Run"
				     :gdbpath "rust-gdb"
                                     :target nil
                                     :cwd nil))

  )

;;;; Language Specific Configuration

;;;;; Haskell
(use-package haskell-mode
  :defer t
  :init
  (add-hook 'haskell-mode-hook #'lsp-deferred)
  (add-hook 'haskell-literate-mode-hook #'lsp-deferred)
  (add-hook 'haskell-mode-hook #'four-space-indent))

;;;;; Rust
(use-package rustic
  :defer t
  :init
  (add-hook 'rustic-mode-hook #'four-space-indent))

;;;;; Nix
(use-package nix-mode
  :defer t
  :init
  (add-hook 'nix-mode-hook #'lsp-deferred))

;;;;; Go
(use-package go-mode
  :defer t
  :init
  (add-hook 'go-mode-hook #'lsp-deferred)
  (add-hook 'go-mode-hook #'enable-tabs)
  :config
  (defun project-find-go-module (dir)
    (when-let ((root (locate-dominating-file dir "go.mod")))
      (cons 'go-module root)))
  (cl-defmethod project-root ((project (head go-module)))
    (cdr project))

  (add-hook 'project-find-functions #'project-find-go-module))

;;;;; Javascript
(use-package js2-mode
  :defer t
  :init
  (add-hook 'js-mode-hook 'js2-minor-mode)
  (add-hook 'js-mode-hook #'lsp-deferred)
  (add-hook 'js-mode-hook 'two-space-indent))

;;;;; Typescript
(use-package typescript-mode
  :defer t
  :init
  (add-hook 'typescript-mode-hook #'lsp-deferred)
  (add-hook 'typescript-mode-hook 'two-space-indent))

;;;;; JSON
(use-package jsonian
  :defer t)

;;;;; Clojure
(use-package cider
  :defer t
  :init
  (add-hook 'clojure-mode-hook 'two-space-indent))

;;;;; Elixir
(use-package elixir-mode
  :defer t)

;;;;; Web Mode
(use-package web-mode
  :defer t)

;;;;; LaTeX
(use-package auctex
  :defer t)

;;;;; Emacs Lisp
(add-hook 'emacs-lisp-mode-hook #'four-space-indent)

;;; init.el ends here
