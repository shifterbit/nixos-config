{pkgs, ...}: {
  programs.emacs = {
    enable = true;
    package = pkgs.emacs-gtk;
  };
  services.emacs.enable = true;

  home.file = {
    ".config/emacs" = {
      source = ./emacs.d;
      recursive = true;
    };
  };
}
