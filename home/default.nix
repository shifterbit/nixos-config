{ ... }:
{
  nixpkgs.config.allowUnfreePredicate = _pkg: true;
  home = let username = "tek"; in
    {
      inherit username;
      homeDirectory = "/home/${username}";
      stateVersion = "22.05";
    };

  imports = [
    ./git.nix
    ./fzf.nix
    ./direnv.nix
    ./shells.nix
    ./neovim
    ./sway
    ./readline.nix
    ./syncthing.nix
    ./environment.nix
    ./develop.nix
    ./theme.nix
    ./i3
    ./dunst
    ./tmux.nix
    ./emacs
  ];
}
