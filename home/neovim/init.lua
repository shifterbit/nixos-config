if vim.fn.has("termguicolors") == 1 then
	vim.cmd("set termguicolors")
end

require("gruvbox").setup({})
vim.cmd("colorscheme gruvbox")
vim.cmd("set number")
vim.cmd("set mouse=a")
vim.cmd("set autowriteall")
vim.cmd("set smartindent")
vim.cmd("set noswapfile")
vim.cmd("set completeopt=longest,menuone,noselect")
vim.cmd('let mapleader = " "')
vim.cmd('let maplocalleader = " l"')
vim.cmd("autocmd FileType elixir setlocal commentstring=#%s")

local opts = { noremap = true, silent = true }
---- Indentation Settings ----
vim.cmd("autocmd FileType * set tabstop=2|set shiftwidth=2|set expandtab")
vim.cmd("autocmd FileType go set tabstop=4|set shiftwidth=4| set noexpandtab")
vim.cmd("autocmd FileType typescript set tabstop=2|set shiftwidth=2|set expandtab")
vim.cmd("autocmd FileType javascript set tabstop=2|set shiftwidth=2|set expandtab")
vim.cmd("autocmd FileType c set tabstop=2|set shiftwidth=2|set expandtab")
vim.cmd("autocmd FileType vue set tabstop=2|set shiftwidth=2|set expandtab")
vim.cmd("autocmd FileType python set tabstop=4|set shiftwidth=4|set expandtab")
vim.cmd("autocmd FileType rust set tabstop=4|set shiftwidth=4|set expandtab")
vim.cmd("autocmd FileType haskell set tabstop=4|set shiftwidth=4|set expandtab")

---- Sane Clipboard ----
vim.api.nvim_set_keymap("v", "<leader>y", '"+y', opts)
vim.api.nvim_set_keymap("n", "<leader>Y", '"+yg_', opts)
vim.api.nvim_set_keymap("n", "<leader>y", '"+y', opts)
vim.api.nvim_set_keymap("n", "<leader>yy", '"+yy', opts)

vim.api.nvim_set_keymap("n", "<leader>p", '"+p', opts)
vim.api.nvim_set_keymap("n", "<leader>P", '"+P', opts)
vim.api.nvim_set_keymap("v", "<leader>p", '"+p', opts)
vim.api.nvim_set_keymap("v", "<leader>P", '"+P', opts)

vim.api.nvim_set_keymap("n", "<leader>d", '"+d', opts)
vim.api.nvim_set_keymap("n", "<leader>D", '"+D', opts)
vim.api.nvim_set_keymap("v", "<leader>d", '"+d', opts)
vim.api.nvim_set_keymap("v", "<leader>D", '"+D', opts)

vim.cmd("let g:netrw_browse_split = 4")
vim.cmd("let g:netrw_winsize = 25")
vim.cmd("let g:netrw_banner = 0")
vim.cmd("let g:netrw_liststyle = 3")

-- Helper Functions

-- Merge tables t1 and t2, overriting values in t1 when there is a conflict
function table.merge(t1, t2)
	for k, v in pairs(t2) do
		t1[k] = v
	end

	return t1
end

-- Puts the given value under key in table t.
-- Returns a new table rather than mutating in place
function table.put(t, key, value)
	local res = table.deepcopy(t)
	res[key] = value
	return res
end

function table.add_description(t, desc)
	return table.put(t, "desc", desc)
end

function table.deepcopy(orig)
	local orig_type = type(orig)
	local copy
	if orig_type == "table" then
		copy = {}
		for orig_key, orig_value in next, orig, nil do
			copy[table.deepcopy(orig_key)] = table.deepcopy(orig_value)
		end
		setmetatable(copy, table.deepcopy(getmetatable(orig)))
	else -- number, string, boolean, etc
		copy = orig
	end
	return copy
end

-- Plugin Setup
vim.cmd([[packadd plenary.nvim]])

vim.cmd([[packadd mini.nvim]])
require("mini.comment").setup()
require("mini.pairs").setup()
require("mini.surround").setup()
require("mini.indentscope").setup()

---- LSP ----
vim.cmd([[packadd nvim-lspconfig]])
local lsp = require("lspconfig")

local add_description = table.add_description

vim.keymap.set("n", "<space>e", vim.diagnostic.open_float, opts)
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
vim.keymap.set("n", "<space>q", vim.diagnostic.setloclist, opts)

local default_on_attach = function(client, bufnr)
	vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")
	-- Mappings.
	local bufopts = { noremap = true, silent = true, buffer = bufnr }

	vim.keymap.set("n", "gD", vim.lsp.buf.declaration, add_description(bufopts, "Goto Declaration"))
	vim.keymap.set("n", "gd", vim.lsp.buf.definition, add_description(bufopts, "Goto Definition"))
	vim.keymap.set("n", "K", vim.lsp.buf.hover, add_description(bufopts, "Hover"))
	vim.keymap.set("n", "<space>gi", vim.lsp.buf.implementation, add_description(bufopts, "Goto Implementation"))
	vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, add_description(bufopts, "Signature Help"))
	vim.keymap.set("n", "<space>wa", vim.lsp.buf.add_workspace_folder, add_description(bufopts, "Add Workspace Folder"))
	vim.keymap.set(
		"n",
		"<space>wr",
		vim.lsp.buf.remove_workspace_folder,
		(add_description(bufopts, "Remove Workspace folder"))
	)
	vim.keymap.set("n", "<space>wl", function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, bufopts)
	vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, add_description(bufopts, "Goto Type Definition"))
	vim.keymap.set("n", "<space>cr", vim.lsp.buf.rename, add_description(bufopts, "Rename"))
	vim.keymap.set("n", "<space>ca", vim.lsp.buf.code_action, add_description(bufopts, "Code Actions"))
	vim.keymap.set("n", "gr", vim.lsp.buf.references, add_description(bufopts, "Find References"))
	vim.keymap.set("n", "<space>cf", function()
		vim.lsp.buf.format({ async = true })
	end, add_description(bufopts, "Format Buffer"))
end

local capabilities = require("cmp_nvim_lsp").default_capabilities()

local default_server_opts = {
	on_attach = default_on_attach,
	capabilities = capabilities,
}

local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

local lua_ls_opts = {
	settings = {
		Lua = {
			runtime = {
				version = "LuaJIT",
				path = runtime_path,
			},
			diagnostics = {
				globals = { "vim" },
			},
			workspace = {
				library = vim.api.nvim_get_runtime_file("", true),
			},
			telemetry = {
				enable = false,
			},
		},
	},
}

local html_opts = {
	cmd = { "vscode-html-language-server", "--stdio" },
	filetypes = { "html", "heex" },
}

local cssls_opts = {
	cmd = { "vscode-css-language-server", "--stdio" },
}

local elixirls_opts = {
	cmd = { "elixir-ls" },
	filetypes = { "elixir", "eelixir", "heex" },
}
local servers = {
	{ name = "bashls", opts = default_server_opts },
	{ name = "clangd", opts = default_server_opts },
	{ name = "pyright", opts = default_server_opts },
	{ name = "tsserver", opts = default_server_opts },
	{ name = "vuels", opts = default_server_opts },
	{ name = "hls", opts = default_server_opts },
	{ name = "gopls", opts = default_server_opts },
	{ name = "clojure_lsp", opts = default_server_opts },
	{ name = "nil_ls", opts = default_server_opts },
	{ name = "rust_analyzer", opts = default_server_opts },
	{ name = "html", opts = table.merge(html_opts, default_server_opts) },
	{ name = "cssls", opts = table.merge(cssls_opts, default_server_opts) },
	{ name = "lua_ls", opts = table.merge(lua_ls_opts, default_server_opts) },
	{ name = "elixirls", opts = table.merge(elixirls_opts, default_server_opts) },
}

for _, server in pairs(servers) do
	lsp[server.name].setup(server.opts)
end

---- Completion ----
vim.cmd([[packadd nvim-cmp]])
vim.cmd([[packadd cmp-path]])
vim.cmd([[packadd cmp-nvim-lsp]])
vim.cmd([[packadd cmp-treesitter]])
vim.cmd([[packadd cmp-nvim-lsp-signature-help]])
vim.cmd([[packadd luasnip]])
vim.cmd([[packadd cmp_luasnip]])
require("luasnip.loaders.from_vscode").lazy_load()

local cmp = require("cmp")
require("cmp").setup({
	snippet = {
		expand = function(args)
			require("luasnip").lsp_expand(args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		["<C-b>"] = cmp.mapping.scroll_docs(-4),
		["<C-f>"] = cmp.mapping.scroll_docs(4),
		["<Tab>"] = cmp.mapping.select_next_item(),
		["<S-Tab>"] = cmp.mapping.select_prev_item(),
		["<C-Space>"] = cmp.mapping.complete(),
		["<C-e>"] = cmp.mapping.abort(),
		["<CR>"] = cmp.mapping.confirm({ select = true }),
	}),
	sources = cmp.config.sources({
		{ name = "nvim_lsp" },
		{ name = "nvim_lsp_signature_help" },
		{ name = "nvim_lua" },
		{ name = "luasnip" },
		{ name = "path" },
		{ name = "treesitter" },
	}),
})

---- Null LSP ----
vim.cmd([[packadd null-ls.nvim]])
local null_ls = require("null-ls")
null_ls.setup({
	sources = {
		null_ls.builtins.formatting.eslint_d,
		null_ls.builtins.diagnostics.eslint_d,
		null_ls.builtins.code_actions.eslint_d,
		null_ls.builtins.formatting.stylua,
		null_ls.builtins.formatting.black,
		null_ls.builtins.formatting.mix.with({ filetypes = { "elixir", "heex" } }),
		null_ls.builtins.diagnostics.credo,
		null_ls.builtins.formatting.alejandra,
		null_ls.builtins.diagnostics.clj_kondo,
		null_ls.builtins.diagnostics.statix,
		null_ls.builtins.code_actions.statix,
		null_ls.builtins.diagnostics.yamllint,
	},
})

---- Syntax ----
vim.cmd([[packadd nvim-treesitter]])
vim.cmd([[packadd nvim-treesitter-context]])
require("nvim-treesitter.configs").setup({
	indent = {
		enable = true,
	},
	highlight = {
		enable = true,
	},
	incremental_selection = {
		enable = true,
	},
})
require("treesitter-context").setup()

---- Which-key ----
vim.cmd([[packadd which-key.nvim]])
require("which-key").setup({})

---- Icons ----
vim.cmd([[packadd nvim-web-devicons]])
require("nvim-web-devicons").setup{}
---- Lualine ----
vim.cmd([[packadd lualine.nvim]])

vim.api.nvim_create_user_command("EnableLualine", function()
	require("lualine").hide({ unhide = true })
end, {})
vim.api.nvim_create_user_command("DisableLualine", function()
	require("lualine").hide()
end, {})
require("lualine").setup({
	sections = {
		lualine_c = {
			{
				"filename",
				file_status = true, -- Displays file status (readonly status, modified status)
				newfile_status = true, -- Display new file status (new file means no write after created)
				path = 1, -- 0: Just the filename
				shorting_target = 40, -- Shortens path to leave 40 spaces in the window
				symbols = {
					modified = "[+]", -- Text to show when the file is modified.
					readonly = "[-]", -- Text to show when the file is non-modifiable or readonly.
					unnamed = "[No Name]", -- Text to show for unnamed buffers.
					newfile = "[New]", -- Text to show for new created file before first writting
				},
			},
		},
	},
})

--- Telescope ---

vim.cmd([[packadd telescope.nvim]])
vim.cmd([[packadd telescope-fzf-native.nvim]])
require("telescope").setup({})
require("telescope").load_extension("fzf")
local builtin = require("telescope.builtin")

vim.keymap.set("n", "<leader><leader>", builtin.commands, table.add_description({}, "Commands"))
vim.keymap.set("n", "<leader>ff", builtin.find_files, table.add_description({}, "Find Files"))
vim.keymap.set("n", "<leader>fr", builtin.live_grep, table.add_description({}, "Search Working Directory"))
vim.keymap.set("n", "<leader>fs", builtin.current_buffer_fuzzy_find, table.add_description({}, "Search Current Buffer"))
vim.keymap.set("n", "<leader>fb", builtin.buffers, table.add_description({}, "Find Buffers"))
vim.keymap.set("n", "<leader>fJ", builtin.lsp_document_symbols, table.add_description({}, "Find Document Symbols"))
vim.keymap.set(
	"n",
	"<leader>fj",
	builtin.lsp_dynamic_workspace_symbols,
	table.add_description({}, "Find Workspace Symbols")
)
