{
  pkgs,
  mypkgs,
  ...
}: {
  programs.neovim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      # General Purpose Plugins
      mini-nvim
      plenary-nvim
      vim-repeat

      # UI
      gruvbox-nvim
      nord-nvim
      which-key-nvim
      lualine-nvim
      nvim-web-devicons


      # Completion
      nvim-cmp
      cmp-path
      cmp-nvim-lsp
      cmp_luasnip
      cmp-treesitter
      cmp-nvim-lsp-signature-help

      # Snippets
      luasnip
      friendly-snippets

      # Telescope
      telescope-nvim
      telescope-fzf-native-nvim

      # LSP
      null-ls-nvim
      nvim-lspconfig

      # Treesitter
      nvim-treesitter.withAllGrammars
      nvim-treesitter-context

    ];
  };
  home.file = {
    ".config/nvim/init.lua" = {
      source = ./init.lua;
    };
  };
}
