{
  pkgs,
  lib,
  config,
  ...
}: {
  xdg.enable = true;
  programs.starship.enable = true;
  home.sessionVariables = {
    RUSTUP_HOME = "${config.xdg.dataHome}/rustup ";
    INPUTRC = "${config.xdg.configHome}/readline/inputrc ";
    NPM_CONFIG_USERCONFIG = "${config.xdg.configHome}/npm/npmrc ";
    _JAVA_OPTIONS = "-Djava.util.prefs.userRoot=${config.xdg.configHome}/java";
    REDISCLI_HISTFILE = "${config.xdg.dataHome}/redis/rediscli_history";
    PSQL_HISTORY = "${config.xdg.dataHome}/psql_history";
    KODI_DATA = "${config.xdg.dataHome}/kodi ";
    JULIA_DEPOT_PATH = "${config.xdg.dataHome}/julia:$JULIA_DEPOT_PATH";
    GRIPHOME = "${config.xdg.configHome}/grip";
    GOPATH = "${config.xdg.dataHome}/go ";
    GNUPGHOME = "${config.xdg.dataHome}/gnupg ";
    GHCUP_USE_XDG_DIRS = "true";
    GEM_SPEC_CACHE = "${config.xdg.cacheHome}/gem";
    DOCKER_CONFIG = "${config.xdg.configHome}/docker ";
    GEM_HOME = "${config.xdg.dataHome}/gem ";
    CARGO_HOME = "${config.xdg.dataHome}/cargo ";
    CABAL_CONFIG = "${config.xdg.configHome}/cabal/config ";
    CABAL_DIR = "${config.xdg.dataHome}/cabal";
    AWS_SHARED_CREDENTIALS_FILE = "${config.xdg.configHome}/aws/credentials";
    AWS_CONFIG_FILE = "${config.xdg.configHome}/aws/config ";
    ANDROID_HOME = "${config.xdg.dataHome}/android ";
    SQLITE_HISTORY = "${config.xdg.cacheHome}/sqlite_history";
    ICEAUTHORITY = "${config.xdg.cacheHome}/ICEauthority";
    LESSHISTFILE = "${config.xdg.cacheHome}/less/history";
    LEIN_HOME = "${config.xdg.dataHome}/lein";
  };
  programs.bash = {
    enable = true;
    historyFile = "${config.xdg.stateHome}/bash/history";
    historyControl = ["erasedups" "ignoredups" "ignorespace"];
    initExtra = ''
      eval `keychain --eval --agents ssh id_rsa`

      [ -n "$EAT_SHELL_INTEGRATION_DIR" ] && \
        source "$EAT_SHELL_INTEGRATION_DIR/bash"
    '';
  };

  home.shellAliases = {
    kitten = "kitty +kitten";
    kssh = "kitty +kitten ssh";
    icat = "kitty +kitten icat";
    lg = "lazygit";
  };

  programs.zoxide.enable = true;
  programs.zsh = {
    enable = false;
    enableAutosuggestions = true;
    enableSyntaxHighlighting = true;
    defaultKeymap = "emacs";
    dotDir = ".config/zsh";
    history = {
      path = "${config.xdg.dataHome}/zsh/zsh_history";
      extended = true;
      ignoreDups = true;
      ignoreSpace = true;
      ignorePatterns = ["exit" "ls" "history" "clear" "fg" "cd"];
    };

    completionInit = "compinit -d ${config.xdg.cacheHome}/zsh/zcompdump-${pkgs.zsh.version}";
    initExtra = ''
      zstyle ':completion:*' menu select
      complete -C aws_completer aws

      [ -n "$EAT_SHELL_INTEGRATION_DIR" ] && \
        source "$EAT_SHELL_INTEGRATION_DIR/zsh"
    '';
    plugins = [
      {
        name = "zsh-completions";
        src = "${pkgs.zsh-completions}/share/zsh/site-functions";
      }
    ];
  };

  programs.fish.enable = true;
  programs.nushell.enable = true;
}
